open Tableur

let test1 () =
  let grille = cree_grille 10 10 in
  ignore grille

let test2 () =
  let grille = cree_grille 10 10 in
  (* Types de base *)
  grille.(0).(0) <- Entier 8 ;
  grille.(1).(0) <- Case(0,0) ;
  grille.(2).(0) <- Case (1,0);
  grille.(0).(1) <- Flottant 3.3 ;
  grille.(4).(4) <- Vide;
  grille.(2).(5) <- Chaine "chaine de charactère" ;
  (* Opérations *)
  grille.(6).(1) <- Unaire { app1 = plus_Un ; operande = Case (2,0) } ;
  grille.(6).(2) <- Binaire { app2 = addition ; gauche = Case (6,1) ; droite = Case (0,1)} ;
  grille.(6).(3) <- Reduction { app = addition ; init = RFlottant 2.2 ; case_debut = (6,1) ; case_fin = (6,2)} ;
  grille.(6).(4) <- Reduction { app = addition ; init = REntier 0 ; case_debut = (6,1) ; case_fin = (6,3)} ;
  (* Erreurs *)
  grille.(8).(8) <- Case (15,5) ;
  grille.(7).(8) <- Case (8,8) ;
  grille.(9).(9) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,8)} ;
  grille.(9).(8) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,9)} ;
  grille.(9).(7) <- Unaire { app1 = plus_Un ; operande = Case (9,6)} ;
  grille.(9).(6) <- Unaire { app1 = plus_Un ; operande = Case (9,7)} ;
  grille.(9).(5) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (9,5) ; case_fin = (9,5)} ;
  grille.(9).(4) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (17,5) ; case_fin = (9,5)} ;
  assert (grille.(0).(0) = Entier 8) ;
  assert (grille.(0).(1) = Flottant 3.3) ;
  assert (grille.(1).(0) = Case (0,0)) ;
  assert (grille.(4).(4) = Vide) ;
  assert (grille.(2).(5) = Chaine "chaine de charactère") ;
  ()

let test3 () =
  let grille = cree_grille 10 10 in
  grille.(0).(1) <- Case (0,2) ;
  grille.(0).(2) <- Case (0,3) ;
  grille.(9).(9) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,8)} ;
  grille.(9).(7) <- Unaire { app1 = plus_Un ; operande = Case (9,6)} ;
  grille.(9).(4) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (0,1) ; case_fin = (0,3)} ;
  assert (cycle grille grille.(0).(1) = false) ;
  assert (cycle grille grille.(9).(9) = false) ;
  assert (cycle grille grille.(9).(7) = false) ;
  assert (cycle grille grille.(9).(4) = false) ;
  grille.(0).(3) <- Case (0,1) ;
  grille.(9).(8) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,9)} ;
  grille.(9).(6) <- Unaire { app1 = plus_Un ; operande = Case (9,7)} ;
  grille.(9).(5) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (9,5) ; case_fin = (9,5)} ;
  assert (cycle grille grille.(0).(1) = true) ;
  assert (cycle grille grille.(9).(9) = true) ;
  assert (cycle grille grille.(9).(7) = true) ;
  assert (cycle grille grille.(9).(5) = true) ;
  assert (cycle grille grille.(9).(4) = true) ;
  ()

let test4 () =
  let grille = cree_grille 10 10 in
  (* Types de base *)
  grille.(0).(0) <- Entier 8 ;
  grille.(1).(0) <- Case(0,0) ;
  grille.(2).(0) <- Case (1,0);
  grille.(0).(1) <- Flottant 3.3 ;
  grille.(4).(4) <- Vide;
  grille.(2).(5) <- Chaine "chaine de charactère" ;
  (* Opérations *)
  grille.(6).(1) <- Unaire { app1 = plus_Un ; operande = Case (2,0) } ;
  grille.(6).(2) <- Binaire { app2 = addition ; gauche = Case (6,1) ; droite = Case (0,1)} ;
  grille.(6).(3) <- Reduction { app = addition ; init = RFlottant 2.2 ; case_debut = (6,1) ; case_fin = (6,2)} ;
  grille.(6).(4) <- Reduction { app = addition ; init = REntier 0 ; case_debut = (6,1) ; case_fin = (6,3)} ;
  (* Erreurs *)
  grille.(8).(8) <- Case (15,5) ;
  grille.(7).(8) <- Case (8,8) ;
  grille.(9).(9) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,8)} ;
  grille.(9).(8) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,9)} ;
  grille.(9).(7) <- Unaire { app1 = plus_Un ; operande = Case (9,6)} ;
  grille.(9).(6) <- Unaire { app1 = plus_Un ; operande = Case (9,7)} ;
  grille.(9).(5) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (9,5) ; case_fin = (9,5)} ;
  grille.(9).(4) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (17,5) ; case_fin = (9,5)} ;
  (* Types de base *)
  assert (eval_expr grille (grille.(0).(0)) = REntier 8) ;
  assert (eval_expr grille (grille.(2).(0)) = REntier 8) ;
  assert (eval_expr grille (grille.(0).(1)) = RFlottant 3.3) ;
  assert (eval_expr grille (grille.(4).(4)) = RVide) ;
  assert (eval_expr grille (grille.(2).(5)) = RChaine "chaine de charactère") ;
  (* Opérations *)
  assert (eval_expr grille (grille.(6).(1)) = REntier 9) ;
  assert (eval_expr grille (grille.(6).(2)) = RFlottant 12.3) ;
  assert (eval_expr grille (grille.(6).(3)) = RFlottant 23.5) ;
  assert (eval_expr grille (grille.(6).(4)) = RFlottant 44.8) ;
  (* Erreurs *)
  assert (eval_expr grille (grille.(8).(8)) = Erreur (Mauvais_indice (15,5))) ;
  assert (eval_expr grille (grille.(7).(8)) = Erreur (Cellule_pointee_en_erreur (8,8))) ;
  assert (eval_expr grille (grille.(9).(9)) = Erreur (Cycle_detecte)) ;
  assert (eval_expr grille (grille.(9).(7)) = Erreur (Cycle_detecte)) ;
  assert (eval_expr grille (grille.(9).(5)) = Erreur (Cycle_detecte)) ;
  assert (eval_expr grille (grille.(9).(4)) = Erreur (Cellule_pointee_en_erreur (17,5))) ;
  ()

let test5 () =
  let grille = cree_grille 10 10 in
  (* Types de base *)
  grille.(0).(0) <- Entier 8 ;
  grille.(1).(0) <- Case(0,0) ;
  grille.(2).(0) <- Case (1,0);
  grille.(0).(1) <- Flottant 3.3 ;
  grille.(4).(4) <- Vide;
  grille.(2).(5) <- Chaine "chaine de charactère" ;
  (* Opérations *)
  grille.(6).(1) <- Unaire { app1 = plus_Un ; operande = Case (2,0) } ;
  grille.(6).(2) <- Binaire { app2 = addition ; gauche = Case (6,1) ; droite = Case (0,1)} ;
  grille.(6).(3) <- Reduction { app = addition ; init = RFlottant 2.2 ; case_debut = (6,1) ; case_fin = (6,2)} ;
  grille.(6).(4) <- Reduction { app = addition ; init = REntier 0 ; case_debut = (6,1) ; case_fin = (6,3)} ;
  (* Erreurs *)
  grille.(8).(8) <- Case (15,5) ;
  grille.(7).(8) <- Case (8,8) ;
  grille.(9).(9) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,8)} ;
  grille.(9).(8) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,9)} ;
  grille.(9).(7) <- Unaire { app1 = plus_Un ; operande = Case (9,6)} ;
  grille.(9).(6) <- Unaire { app1 = plus_Un ; operande = Case (9,7)} ;
  grille.(9).(5) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (9,5) ; case_fin = (9,5)} ;
  grille.(9).(4) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (17,5) ; case_fin = (9,5)} ;
  let res = eval_grille grille in
  (* Types de base *)
  assert (res.(0).(0) = REntier 8) ;
  assert (res.(2).(0) = REntier 8) ;
  assert (res.(0).(1) = RFlottant 3.3) ;
  assert (res.(4).(4) = RVide) ;
  assert (res.(2).(5) = RChaine "chaine de charactère") ;
  (* Opérations *)
  assert (res.(6).(1) = REntier 9) ;
  assert (res.(6).(2) = RFlottant 12.3) ;
  assert (res.(6).(3) = RFlottant 23.5) ;
  assert (res.(6).(4) = RFlottant 44.8) ;
  (* Erreurs *)
  assert (res.(8).(8) = Erreur (Mauvais_indice (15,5))) ;
  assert (res.(7).(8) = Erreur (Cellule_pointee_en_erreur (8,8))) ;
  assert (res.(9).(9) = Erreur (Cycle_detecte)) ;
  assert (res.(9).(7) = Erreur (Cycle_detecte)) ;
  assert (res.(9).(5) = Erreur (Cycle_detecte)) ;
  assert (res.(9).(4) = Erreur (Cellule_pointee_en_erreur (17,5))) ;
  ()

let test6 () =
  let grille = cree_grille 10 10 in
  (* Types de base *)
  grille.(0).(0) <- Entier 8 ;
  grille.(1).(0) <- Case(0,0) ;
  grille.(2).(0) <- Case (1,0);
  grille.(0).(1) <- Flottant 3.3 ;
  grille.(4).(4) <- Vide;
  grille.(2).(5) <- Chaine "chaine de charactère" ;
  (* Opérations *)
  grille.(6).(1) <- Unaire { app1 = plus_Un ; operande = Case (2,0) } ;
  grille.(6).(2) <- Binaire { app2 = addition ; gauche = Case (6,1) ; droite = Case (0,1)} ;
  grille.(6).(3) <- Reduction { app = addition ; init = RFlottant 2.2 ; case_debut = (6,1) ; case_fin = (6,2)} ;
  grille.(6).(4) <- Reduction { app = addition ; init = REntier 0 ; case_debut = (6,1) ; case_fin = (6,3)} ;
  (* Erreurs *)
  grille.(8).(8) <- Case (15,5) ;
  grille.(7).(8) <- Case (8,8) ;
  grille.(9).(9) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,8)} ;
  grille.(9).(8) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,9)} ;
  grille.(9).(7) <- Unaire { app1 = plus_Un ; operande = Case (9,6)} ;
  grille.(9).(6) <- Unaire { app1 = plus_Un ; operande = Case (9,7)} ;
  grille.(9).(5) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (9,5) ; case_fin = (9,5)} ;
  grille.(9).(4) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (17,5) ; case_fin = (9,5)} ;
  affiche_grille grille ;
  let res = eval_grille grille in
  affiche_grille_resultat res ;
  ()

let test7 () =
  let grille = cree_grille 10 10 in
  (*Base*)
  grille.(0).(0) <- Entier (-8) ;
  grille.(0).(1) <- Flottant 3.3 ;
  grille.(0).(2) <- Entier 7 ;
  grille.(0).(3) <- Chaine "number : " ;
  (*Opération Unaire*)
  grille.(1).(0) <- (abs (Case (0,0))) ;
  grille.(1).(1) <- (inv (Case (1,0))) ;
  grille.(1).(2) <- (opp (Case (0,0))) ;
  grille.(1).(3) <- (plus (Case (0,0))) ;
  grille.(1).(4) <- (fact (Case (1,0))) ;
  grille.(1).(5) <- (carr (Case (0,0))) ;
  grille.(1).(6) <- (racine (Case (1,0))) ;
  (*Opération Binaire*)
  grille.(2).(0) <- (add (Case (0,0)) (Case (0,1))) ;
  grille.(2).(1) <- (mult (Case (0,0)) (Case (1,0))) ;
  grille.(2).(2) <- (maxi (Case (0,0)) (Case (0,1))) ;
  grille.(2).(3) <- (puis (Case (1,0)) (Case (0,1))) ;
  grille.(2).(4) <- (concat (Case (0,3)) (Case (0,2))) ;
  (*Opération N-naire*)
  grille.(3).(0) <- (somme (0,0) (0,2)) ;
  grille.(3).(1) <- (moy (0,0) (0,2)) ;
  grille.(3).(2) <- (multi_all (0,0) (0,2)) ;
  grille.(3).(3) <- (concat_all (0,0) (0,3)) ;
  let res = eval_grille grille in
  (*Opération Unaire*)
  assert (res.(1).(0) = REntier 8) ;
  assert (res.(1).(1) = REntier (-8)) ;
  assert (res.(1).(2) = RFlottant ((-1. /. 8.))) ;
  assert (res.(1).(3) = REntier (-7)) ;
  assert (res.(1).(4) = REntier (40320)) ;
  assert (res.(1).(5) = REntier (64)) ;
  assert (res.(1).(6) = RFlottant (sqrt 8.)) ;
  (*Opération Unaire*)
  assert (res.(2).(0) = RFlottant (-4.7)) ;
  assert (res.(2).(1) = REntier (-64)) ;
  assert (res.(2).(2) = RFlottant 3.3) ;
  assert (res.(2).(3) = RFlottant (8.**3.3)) ;
  assert (res.(2).(4) = RChaine "number : 7") ;
  (*Opération Unaire*)
  assert (res.(3).(0) = RFlottant 2.3) ;
  assert (res.(3).(1) = RFlottant (2.3 /. 3.));
  assert (res.(3).(2) = RFlottant (-8. *. 3.3 *. 7.)) ;
  assert (res.(3).(3) = RChaine "-83.37number : ") ;
  ()

let test8 () =
  let grille = cree_grille 10 10 in
  (* Types de base *)
  grille.(0).(0) <- Entier 8 ;
  grille.(1).(0) <- Case(0,0) ;
  grille.(2).(0) <- Case (1,0);
  grille.(0).(1) <- Flottant 3.3 ;
  grille.(4).(4) <- Vide;
  grille.(2).(5) <- Chaine "chaine de charactère" ;
  (* Opérations *)
  grille.(6).(1) <- Unaire { app1 = plus_Un ; operande = Case (2,0) } ;
  grille.(6).(2) <- Binaire { app2 = addition ; gauche = Case (6,1) ; droite = Case (0,1)} ;
  grille.(6).(3) <- Reduction { app = addition ; init = RFlottant 2.2 ; case_debut = (6,1) ; case_fin = (6,2)} ;
  grille.(6).(4) <- Reduction { app = addition ; init = REntier 0 ; case_debut = (6,1) ; case_fin = (6,3)} ;
  (* Erreurs *)
  grille.(8).(8) <- Case (15,5) ;
  grille.(7).(8) <- Case (8,8) ;
  grille.(9).(9) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,8)} ;
  grille.(9).(8) <- Binaire { app2 = addition ; gauche = Case (0,0) ; droite = Case (9,9)} ;
  grille.(9).(7) <- Unaire { app1 = plus_Un ; operande = Case (9,6)} ;
  grille.(9).(6) <- Unaire { app1 = plus_Un ; operande = Case (9,7)} ;
  grille.(9).(5) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (9,5) ; case_fin = (9,5)} ;
  grille.(9).(4) <- Reduction { app = addition ; init = REntier 4 ; case_debut = (17,5) ; case_fin = (9,5)} ;
  let mem = eval_grille_mem grille in
  let res = eval_grille grille in
  assert (res = mem) ;
  ()


let run_tests () =
  let liste_tests =
    [("création grille", test1); ("affectation grille", test2) ; ("detection cycle", test3) ;
     ("eval_expr", test4) ;  ("eval_grille", test5) ; ("affichages grille et res", test6)
    ; ("opérations_expressions", test7) ; ("memoïsation", test8)(* ... *)]
  in
  List.iteri
    (fun i (nom_test, f_test) ->
      Format.printf "Test #%d - %s:\t" (i + 1) nom_test ;
      try
        f_test () ;
        Format.printf "\027[32mOk\n\027[39m"
      with exn ->
        Format.printf "\027[31mErreur - %s\n\027[39m" (Printexc.to_string exn))
    liste_tests

(* Main *)
let () = run_tests ()
