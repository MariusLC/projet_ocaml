(** Types *)
type expr =
  | Vide
  | Chaine of string
  | Entier of int
  | Flottant of float
  | Case of int * int
  | Unaire of op_unaire
  | Binaire of op_binaire
  | Reduction of op_reduction

and op_unaire = { app1 : resultat->resultat ; operande : expr }
and op_binaire = { app2 : resultat->resultat->resultat ; gauche : expr ; droite : expr }
and op_reduction = { app : resultat->resultat->resultat ; init : resultat ; case_debut : int * int ; case_fin : int * int }

and resultat =
  | RVide
  | RChaine of string
  | REntier of int
  | RFlottant of float
  | Erreur of erreur

and erreur =
  | Mauvais_indice of (int * int)
  | Cycle_detecte
  | Cellule_pointee_en_erreur of (int * int)
  | Res_type_1 of resultat
  | Res_type_2 of (resultat * resultat)
  | Res_type_N of resultat
  | Sqrt_Negative of resultat
  | Fact_Negative of resultat
  | AutreErreur

exception Exception_Mauvais_indice of (int * int)
exception Res_type
exception Pas_une_case
exception Pas_encore_implemente of string

type grille = expr array array

(** Fonctions *)

let cree_grille _i _j = Array.make_matrix _i _j Vide

let expr_to_string exp =
  match exp with
  | Vide -> "[]"
  | Chaine c -> c
  | Entier e -> string_of_int e
  | Flottant f -> string_of_float f
  | Case (a,b) -> "@("^ string_of_int a ^ "," ^string_of_int b ^")"
  | Unaire _ -> "Unaire"
  | Binaire _ -> "Binaire"
  | Reduction _ -> "Reduction"

let affiche_grille g =
  Format.printf "\n";
  Array.iter (fun e ->
      Array.iter (fun e -> let s = expr_to_string e in
                   if (String.length s >= 10) then Format.printf "|%-10s" (String.sub s 0 10)
                   else Format.printf "|%-10s" s)
        e; Format.printf "|\n") g

let rec res_to_string res =
  let erreur_to_string err =
    match err with
    | Mauvais_indice (x,y) -> "OUT("^ string_of_int x ^ "," ^string_of_int y ^")"
    | Cycle_detecte -> "CYCLE"
    | Cellule_pointee_en_erreur (x,y) -> "POINT("^ string_of_int x ^ "," ^string_of_int y ^")"
    | Res_type_1 res -> "TYPE1{"^(res_to_string res)^"}"
    | Res_type_2 (e1, e2) -> "TYPE2{"^(res_to_string e1)^";"^(res_to_string e2)^"}"
    | Res_type_N res -> "TYPEN{"^(res_to_string res)^"}"
    | Sqrt_Negative res -> "SqrtNeg "^(res_to_string res)
    | Fact_Negative res -> "!Neg "^(res_to_string res)
    | AutreErreur -> "AUTREERREUR"
  in
  match res with
  | RVide -> "[]"
  | RChaine c -> c
  | REntier e -> string_of_int e
  | RFlottant f -> string_of_float f
  | Erreur err -> erreur_to_string err

let affiche_grille_resultat g =
  Format.printf "\n";
  Array.iter (fun e ->
      Array.iter (fun e -> let s = res_to_string e in
                   if (String.length s >= 10) then Format.printf "|%-10s" (String.sub s 0 10)
                   else Format.printf "|%-10s" s)
        e; Format.printf "|\n") g


module ExprSet = Set.Make( struct
    let compare = compare
    type t = int*int
  end)

let indices_out grille (x,y) = (x > (Array.length grille - 1)) || (y > (Array.length grille.(0) - 1)) || (x < 0) || (y < 0)

let cycle grille expr =
  let rec aux expr acc =
    let reduce o acc =
      (*  reduce évalue de haut en bas puis de gauche à droite  *)
      match o.case_debut, o.case_fin with
      | (x1, y1),(x2, y2) ->
      let rec aux1 i j acc =
        if (aux (grille.(i).(j)) (ExprSet.add (i,j) acc)) then true
        else
          if i = x2 then
            if j = y2 then false
            else aux1 x1 (j+1) acc
          else aux1 (i+1) j acc
      in aux1 x1 y1 acc
    in
    match expr with
    | Case (x,y) -> if indices_out grille (x,y) then raise (Exception_Mauvais_indice (x,y))
      else if ExprSet.mem (x,y) acc then true
      else aux (grille.(x).(y)) (ExprSet.add (x,y) acc)
    | Unaire o -> aux o.operande acc
    | Binaire o -> if aux o.gauche acc then true else aux o.droite acc
    | Reduction o -> if (indices_out grille o.case_debut) then raise (Exception_Mauvais_indice o.case_debut)
      else if (indices_out grille o.case_fin) then raise (Exception_Mauvais_indice o.case_fin)
        else if ExprSet.mem o.case_debut acc then true else reduce o acc
    | _ -> false
  in aux expr ExprSet.empty

let eval_expr grille expr =
  try
    if cycle grille expr then (Erreur (Cycle_detecte)) else
      let rec aux grille expr =
        let reduce grille o =
          (*  reduce évalue de haut en bas puis de gauche à droite  *)
          match o.case_debut, o.case_fin with
          | (x1, y1),(x2, y2) ->
            let rec aux1 res i j =
              if i = x2 then
                if j = y2 then (o.app res (aux grille grille.(i).(j)))
                  else aux1 (o.app res (aux grille grille.(i).(j))) x1 (j+1)
              else aux1 (o.app res (aux grille grille.(i).(j))) (i+1) j
            in aux1 o.init x1 y1
        in
        match expr with
        | Case (x,y) -> aux grille grille.(x).(y)
        | Unaire o -> let e = aux grille o.operande in o.app1 e
        | Binaire o -> let g = aux grille o.gauche in let d = aux grille o.droite in o.app2 g d
        | Reduction o -> reduce grille o
        | Vide -> RVide
        | Chaine c -> RChaine c
        | Entier e -> REntier e
        | Flottant f -> RFlottant f
      in aux grille expr
  with Exception_Mauvais_indice (x,y) ->
  match expr with
  | Case (a,b)-> if (a,b) = (x,y) then Erreur (Mauvais_indice (x,y)) else Erreur (Cellule_pointee_en_erreur (a,b))
  | _ -> Erreur (Cellule_pointee_en_erreur (x,y))

let eval_grille g =
  Array.map (fun e -> Array.map (fun e -> (eval_expr g e)) e) g


(* Memoïsation *)
let test_mem grille_r ex ey res =
  if grille_r.(ex).(ey) = RVide then ((grille_r.(ex).(ey) <- res) ; res) else grille_r.(ex).(ey)

let eval_expr_mem grille_e grille_r expr ex ey =
  try
    if cycle grille_e expr then test_mem grille_r ex ey (Erreur (Cycle_detecte)) else
      let rec aux expr eex eey =
        let reduce grille_e o =
          (*  reduce évalue de haut en bas puis de gauche à droite  *)
          match o.case_debut, o.case_fin with
          | (x1, y1),(x2, y2) ->
            let rec aux1 res i j =
              if i = x2 then
                if j = y2 then (o.app res (test_mem grille_r i j (aux grille_e.(i).(j) i j)))
                else  aux1 (o.app res (test_mem grille_r i j (aux grille_e.(i).(j) i j))) x1 (j+1)
              else aux1 (o.app res (test_mem grille_r i j (aux grille_e.(i).(j) i j))) (i+1) j
            in aux1 o.init x1 y1
        in
        match expr with
        | Case (x,y) -> test_mem grille_r x y (aux grille_e.(x).(y) x y)
        | Unaire o -> let e = aux o.operande ex ey in test_mem grille_r eex eey (o.app1 e)
        | Binaire o -> let g = aux o.gauche ex ey in let d = aux o.droite ex ey in test_mem grille_r eex eey (o.app2 g d)
        | Reduction o -> test_mem grille_r eex eey (reduce grille_e o)
        | Vide -> test_mem grille_r eex eey RVide
        | Chaine c -> test_mem grille_r eex eey (RChaine c)
        | Entier e -> test_mem grille_r eex eey (REntier e)
        | Flottant f -> test_mem grille_r eex eey (RFlottant f)
      in aux expr ex ey
  with Exception_Mauvais_indice (x,y) ->
  match expr with
  | Case (a,b)-> if (a,b) = (x,y) then test_mem grille_r ex ey (Erreur (Mauvais_indice (x,y)))
    else test_mem grille_r ex ey (Erreur (Cellule_pointee_en_erreur (a,b)))
  | _ -> test_mem grille_r ex ey (Erreur (Cellule_pointee_en_erreur (x,y)))

let eval_grille_mem g =
  let dim_x = (Array.length g) in
  let dim_y = (Array.length g.(0)) in
  let aux grille_res =
    for i = 0 to (dim_x-1) do
      for j = 0 to (dim_y-1) do
        if grille_res.(i).(j) = RVide then (grille_res.(i).(j) <- eval_expr_mem g grille_res (g.(i).(j)) i j)
      done
    done
    ;grille_res
  in aux (Array.make_matrix dim_x dim_y RVide)

(*    opérations Unaires :    *)
let val_abs res =
  match res with
  | REntier e -> if (e < 0) then REntier (e * (-1)) else REntier e
  | RFlottant f -> if (f < 0.0) then RFlottant (f *. (-1.0)) else RFlottant f
  | _ -> Erreur (Res_type_1 res)

let inverse res =
  match res with
  | REntier e -> REntier (e * (-1))
  | RFlottant f -> RFlottant (f *. (-1.0))
  | _ -> Erreur (Res_type_1 res)

let plus_Un res =
  match res with
  | REntier e -> REntier (e + 1)
  | RFlottant f -> RFlottant (f +. 1.0)
  | _ -> Erreur (Res_type_1 res)

let oppose res =
  match res with
  | REntier e -> RFlottant (1. /. (float_of_int e))
  | RFlottant f -> RFlottant (1. /. f)
  | _ -> Erreur (Res_type_1 res)

let carre res =
  match res with
  | REntier e -> REntier (int_of_float ((float_of_int e)**2.))
  | RFlottant f -> RFlottant (f**2.)
  | _ -> Erreur (Res_type_1 res)

let racine_carre res =
  match res with
  | REntier e -> if e >= 0 then RFlottant (sqrt (float_of_int e)) else Erreur (Sqrt_Negative res)
  | RFlottant f -> if f >= 0. then RFlottant (sqrt f) else Erreur (Sqrt_Negative res)
  | _ -> Erreur (Res_type_1 res)

let factorielle res =
  let rec aux n =
    match n with
    | 0 | 1 ->  1
    | n -> n * (aux (n - 1))
  in
  match res with
  | REntier e -> if e >= 0 then REntier (aux e) else Erreur (Fact_Negative res)
  | _ -> Erreur (Res_type_1 res)

(*    opérations Binaires :    *)
let addition g d =
  match g,d with
  | REntier eg, REntier ed-> REntier (eg+ed)
  | RFlottant fg, RFlottant fd -> RFlottant (fd +. fg)
  | REntier e, RFlottant f | RFlottant f, REntier e -> RFlottant (float_of_int e +. f)
  | _, _ -> Erreur (Res_type_2 (g, d))

let multiplication g d =
  match g,d with
  | REntier eg, REntier ed-> REntier (eg * ed)
  | RFlottant fg, RFlottant fd -> RFlottant (fd *. fg)
  | REntier e, RFlottant f | RFlottant f, REntier e -> RFlottant (float_of_int e *. f)
  | _, _ -> Erreur (Res_type_2 (g, d))

let maximum g d =
  match g,d with
  | REntier eg, REntier ed-> if (eg < ed) then REntier ed else REntier eg
  | RFlottant fg, RFlottant fd -> if (fg < fd) then RFlottant fd else RFlottant fg
  | REntier e, RFlottant f | RFlottant f, REntier e -> if ((float_of_int e) < f) then RFlottant f else REntier e
  | _, _ -> Erreur (Res_type_2 (g, d))

let puissance ni pu =
  match ni, pu with
  | REntier n, REntier p -> RFlottant ((float_of_int n)**(float_of_int p))
  | RFlottant n, REntier p -> RFlottant (n ** (float_of_int p))
  | REntier n, RFlottant p -> RFlottant ((float_of_int n) ** p)
  | RFlottant n, RFlottant p -> RFlottant (n ** p)
  | _, _ -> Erreur (Res_type_2 (ni, pu))

let concatenation c1 c2 =
  match c1, c2 with
  | RChaine s1, RChaine s2 -> RChaine (s1^s2)
  | RChaine c, REntier e -> RChaine (c^(string_of_int e))
  | REntier e, RChaine c -> RChaine ((string_of_int e)^c)
  | RChaine c, RFlottant f -> RChaine (c^(string_of_float f))
  | RFlottant f, RChaine c -> RChaine ((string_of_float f)^c)
  | RChaine c, RVide | RVide, RChaine c -> RChaine c
  | _, _ -> Erreur (Res_type_2 (c1, c2))

(* Obligatoire pour faire une moyenne *)
let nb_cases_rectangle (x1,y1) (x2,y2) = (((max x1 x2)-(min x1 x2)+1)*((max y1 y2)-(min y1 y2)+1))

(* Autes opérations *)
let abs v = Unaire { app1 = val_abs ; operande = v }
let inv v = Unaire { app1 = inverse ; operande = v }
let opp v = Unaire { app1 = oppose ; operande = v }
let plus v = Unaire { app1 = plus_Un ; operande = v }
let fact v = Unaire { app1 = factorielle ; operande = v }
let carr v = Unaire { app1 = carre ; operande = v }
let racine v = Unaire { app1 = racine_carre ; operande = v }

let add a b = Binaire { app2 = addition ; gauche = a ; droite = b }
let mult a b = Binaire { app2 = multiplication ; gauche = a ; droite = b }
let maxi a b = Binaire { app2 = maximum ; gauche = a ; droite = b }
let puis n p = Binaire { app2 = puissance ; gauche = n ; droite = p }
let concat c1 c2 = Binaire { app2 = concatenation ; gauche = c1 ; droite = c2 }

let somme c_d c_f = Reduction { app = addition ; init = REntier 0 ; case_debut = c_d ; case_fin = c_f }
let moy c_d c_f = Binaire {app2 = multiplication ;
                           gauche = Flottant (1.0/.(float_of_int (nb_cases_rectangle c_d c_f))) ;
                           droite = Reduction { app = addition ; init = REntier 0 ; case_debut = c_d ; case_fin = c_f }}
let multi_all c_d c_f = Reduction { app = multiplication ; init = REntier 1 ; case_debut = c_d ; case_fin = c_f }
let concat_all c_d c_f = Reduction { app = concatenation ; init = RChaine "" ; case_debut = c_d ; case_fin = c_f }

(*Q9*)
(* Référencer une case à l’aide d’une position relative à la sienne *)
let case_ref dir_x dir_y x y =
  Case ( (x + dir_x) , (y + dir_y) )
